Trying to follow best practices on this, using this as a guide:

https://www.andrewhoog.com/post/git-submodule-for-hugo-themes/

Because I'm on GitLab, I had to create a "mirror" repository on GitLab
that pulled the upstream project from GitHub.  "hugo-xmin-mirror"

Then I forked the mirror on GitLab so I had my own repo "hugo-xmin-jhealy".

Finally, I added that as a submodule to this blog repo.

Should theoretically be able to sync the mirror, then pull the upstream changes
to the fork (in the submodule).  I'm not sure how actively developed the
upstream is, but on the off-chance that it is we can go from there.
